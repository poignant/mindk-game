(function (window) {
    var CANVAS_WIDTH  = 480;
    var CANVAS_HEIGHT = 320;
    var FPS           = 30;

    var canvasElement = $("<canvas width='" + CANVAS_WIDTH +
        "' height='" + CANVAS_HEIGHT + "'></canvas>");
    var canvas        = canvasElement.get(0).getContext("2d");
    canvasElement.appendTo('body');

    function extend(Child, Parent) {
        var F                       = function () {
        };
        F.prototype                 = Parent.prototype;
        Child.prototype             = new F();
        Child.prototype.constructor = Child;
        Child.superclass            = Parent.prototype;
    }

    function Base(ctx, sprite, x, y) {
        this.ctx    = ctx;
        this.sprite = sprite;
        this.x      = x;
        this.y      = y;
    }

    Base.prototype.draw    = function () {
        this.sprite.draw(this.ctx, this.x, this.y);
    };
    Base.prototype.update  = function () {
        this.x += this.xVelocity;
        this.y += this.yVelocity;
        if (this.y < 10) {
            this.explode();
            this.active = false;
        }
    };
    Base.prototype.explode = function () {
        console.log('EXPLODE!!!');
    };

    function Player(ctx, sprite, x, y) {
        Base.apply(this, arguments);
        this.width  = 20;
        this.height = 20;

        this.draw = function () {
            Base.prototype.draw.call(this);
            this.bullets.forEach(function (bullet) {
                bullet.update();
                bullet.draw();
            });
            this.bullets = this.bullets.filter(function (bullet) {
                return bullet.active;
            });
        };

        this.bullets = [];

        this.midpoint = function () {
            return {
                x: this.x + this.width / 2,
                y: this.y + this.height / 2
            };
        };
        this.shoot    = function () {
            var bulletPosition = this.midpoint();
            this.bullets.push(new Bullet(canvas, Sprite("circle"), bulletPosition.x, bulletPosition.y));
        }
    }

    function Bullet(ctx, sprite, x, y) {
        Base.apply(this, arguments);
        this.xVelocity = 0;
        this.yVelocity = -10;
        this.width     = 10;
        this.height    = 10;
        this.active    = true;
    }

    function Enemy(ctx, sprite, x, y) {
        Base.apply(this, arguments);
        this.age       = Math.floor(Math.random() * 128);
        this.xVelocity = 3 * Math.sin(this.age * Math.PI / 64);
        this.yVelocity = 2;
        this.width     = 10;
        this.height    = 10;
        this.active    = true;
    }

    function Game(player, Enemy, ctx) {
        this.player  = player;
        this.enemies = [];
        this.Enemy   = Enemy;
        this.ctx     = ctx;

        this.handleCollisions = function () {

            var self = this;

            function collides(a, b) {
                return a.x < b.x + b.width &&
                    a.x + a.width > b.x &&
                    a.y < b.y + b.height &&
                    a.y + a.height > b.y;
            }

            self.player.bullets.forEach(function (bullet) {
                self.enemies.forEach(function (enemy) {
                    if (collides(bullet, enemy)) {
                        enemy.explode();
                        enemy.active  = false;
                        bullet.active = false;
                    }
                });
            });
        };
        this.draw             = function draw() {
            canvas.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
            this.player.draw();

            this.enemies.forEach(function (enemy) {
                enemy.update();
                enemy.draw();
            });

            this.enemies = this.enemies.filter(function (enemy) {
                return enemy.active;
            });
        };
        this.update           = function update() {
            if (keydown.space) {
                this.player.shoot();
            }

            if (keydown.left) {
                this.player.x -= 5;
            }

            if (keydown.right) {
                this.player.x += 5;
            }

            this.player.x = this.player.x.clamp(0, CANVAS_WIDTH - this.player.width);
            if (Math.random() * 100 > 50) {
                this.enemies.push(new this.Enemy(ctx, Sprite("bubble-2"), Math.random() * 1000, 10));
            }
            this.handleCollisions();
        };
        this.init             = function () {
            var self = this;
            setInterval(function () {
                self.update();
                self.draw();
            }, 1000 / FPS);
        }
    }

    extend(Player, Base);
    extend(Bullet, Base);
    extend(Enemy, Base);

    var player = new Player(canvas, Sprite("bottle"), 50, 270);
    var game = new Game(player, Enemy, canvas);
    game.init();

})(window);

